import pkg from './package'

export default {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto&display=swap' },
      { rel: 'stylesheet', href: '//cdn.materialdesignicons.com/4.5.95/css/materialdesignicons.min.css' },
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/global.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src:'~/plugins/external_components.js', ssr: false },
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    ['@nuxtjs/axios'],
    ['@nuxtjs/proxy', { pathRewrite: { '^/api' : '/api/plan/v1' } }],
  ],

  // Axios Config
  axios: {
    withCredentials: true,
    prefix: '/api/plan/v1/',
    proxy: true,
  },

  // Nuxt Proxy Config
  proxy: {
    '/api/plan/v1/': 'https://1stquest.com',
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
