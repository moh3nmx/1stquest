export default function ({ store, redirect }) {
  if (!store.state.selectedFilters) {
    redirect('/')
  }
}
