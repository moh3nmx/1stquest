import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
export const strict = false
export const state = () => ({
  selectedFilters: null
});

export const actions = {

};

export const mutations = {
  SET_FILTERS(state, filters){
    state.selectedFilters = filters
  }
};
